const data = require( "./data" );

module.exports = {
    raw : data,
    get : () => data[Math.floor(Math.random()*data.length)]
};

